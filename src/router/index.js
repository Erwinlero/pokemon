import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/favoritos',
      name: 'favoritos',
      component: () => import('../views/Favoritos.vue')
    },
    {
      path: '/pokemones',
      name: 'pokemones',
      component: () => import('../views/Pokemones.vue')
    }
  ]
})

export default router
