import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue3} from 'bootstrap-vue-3'
import {store} from './store/store'
import './assets/main.css'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(BootstrapVue3)

app.mount('#app')
