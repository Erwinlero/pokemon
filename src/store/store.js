import { createApp } from "vue";
import { config } from "./../config/config";
import { createStore } from "vuex";
import axios from "axios";
// Create a new store instance.
export const store = createStore({
  state() {
    return {
      api_url: `${config.path_api}/pokemon?offset=0&limit=10`,
      next_url: null,
      prev_url: null,
      list: [],
      list_two: [],
      favorites: [],
      limit: 10,
      total: 0,
      isLoading: false,
      page:1    };
  },
  mutations: {
    success(state, data) {
      state.next_url = data.next;
      state.page = data.page;
      state.prev_url = data.previous;
      if(data.list_one) state.list = data.list_one;
      if(data.list_two) state.list_two = data.list_two;
      state.total = data.count;
    },
    add_favorite(state,data){
      state.favorites.push(data)    }
  },
  actions: {
    async getPokemones({ state, commit }, payload) {
      let url = state.api_url;

      if(payload.list=='list_two' && payload.limit && payload.page){
        let offset = (payload.page-1)*payload.limit
        url = `${config.path_api}/pokemon?offset=${offset}&limit=${payload.limit}`
      }else{
        if (payload && payload.next_url) url = payload.next_url;
        if (payload && payload.prev_url) url = payload.prev_url;  
      }
      try {
        state.isLoading = true;
        const { data } = await axios.get(url);
        for (const pokemon of data.results) {
          const detail = await axios.get(pokemon.url);
          pokemon.pokemon = detail.data;
        }
      if(payload.list== 'list_one') data.list_one = data.results
      if(payload.list== 'list_two') data.list_two = data.results
        commit("success", data);
      } catch (e) {
        commit("success", []);
      } finally {
        state.isLoading = false;
      }
    },
    addFavorites({commit},pokemon){
       commit("add_favorite",pokemon) 
    }
  },
  getters: {
    IS_LOADING: (state) => state.isLoading,
  },
});
